﻿#pragma strict


//var snd : AudioClip;
var BulletEffect : Transform;
var BulletExplosion : Transform;
function Awake()
{
	//AudioSource.PlayClipAtPoint(snd, transform.position);
	Instantiate(BulletEffect, transform.position, Quaternion.identity);
}
function OnTriggerEnter(coll : Collider)
{	
	Instantiate(BulletExplosion, transform.position, Quaternion.identity);
	Destroy(gameObject);
}