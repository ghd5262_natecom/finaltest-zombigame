﻿#pragma strict
var speed  = 30;
var rotSpeed = 300;
var gun : GameObject;
static var characterPos : Vector3;
var power = 1000;
var bullet : Transform;
var boom : Transform;
static var boomNum : int;

function Awake(){
	boomNum = 5;
}
function OnTriggerEnter(coll : Collider)
{
	if(coll.gameObject.tag.Substring(0,5) == "ENEMY")
	{
		if(jsScore.state < 5)
		{
			Destroy(gameObject);
			Application.LoadLevel("LoseGame");
		}
	}
}
function Update () {
	characterPos = transform.position;
	var amtToMove = speed * Time.deltaTime;
	//var amtToRot = rotSpeed * Time.deltaTime;
	
	var front = Input.GetAxis("Vertical");
	var side = Input.GetAxis("Horizontal");
	
	
	transform.Translate(Vector3.forward * front * amtToMove);
	transform.Translate(Vector3.right * side * amtToMove);
	///transform.Rotate(Vector3(0, ang * amtToRot, 0));
	
	if(Input.GetMouseButton(0)){
		var spPoint1 = GameObject.Find("SpawnPoint1");
		var myBullet = Instantiate(bullet, spPoint1.transform.position, Quaternion.identity);
		myBullet.rigidbody.AddForce(spPoint1.transform.forward * power);
		
	}
	
	if(Input.GetButtonDown("Fire2")){
		if(boomNum > 0){
			var spPoint2 = GameObject.Find("SpawnPoint2");
			var myBoom = Instantiate(boom, spPoint2.transform.position, Quaternion.identity);
			myBoom.rigidbody.AddForce(spPoint2.transform.forward * power);
			boomNum--;
		}
	}
	
}