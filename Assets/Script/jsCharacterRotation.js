﻿#pragma strict

function Update () {

	var ray : Ray = Camera.main.ScreenPointToRay(Input.mousePosition);
	var hit : RaycastHit;

	Physics.Raycast(ray, hit);
	//print(hit.point.x);print(hit.point.z);
	var v3 : Vector3;
	
	var angle : float;
	angle = (Mathf.Atan2(hit.point.z - jsCharacterMove.characterPos.z, hit.point.x - jsCharacterMove.characterPos.x));//+ (3.14 *(-0.5f)));
	
	//v3 = jsCharacterMove.characterPos - hit.point;
	//v3 = Vector3.Normalize(v3);
	
	// Look at and dampen the rotation
	var pos1 : Vector3 = new Vector3(hit.point.x, .0f, hit.point.z);
	var pos2 : Vector3 = new Vector3(transform.position.x, .0f, transform.position.z);
	var rotation = Quaternion.LookRotation(pos1-pos2);//(hit.point - transform.position);
	transform.rotation = rotation;
}