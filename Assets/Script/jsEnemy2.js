﻿#pragma strict
var player : GameObject;
var snd : AudioClip;

static var enemystate2 : int;
function Start () {
	player = GameObject.FindGameObjectWithTag( "PLAYER" );
	enemystate2 = 20;
}

function Update() {
	transform.LookAt(player.transform);
}

function OnTriggerEnter(coll : Collider)
{
	if(coll.gameObject.tag == "PLAYER"){
		jsScore.state -= 20;
		Destroy(gameObject);
	}
	if(coll.gameObject.tag == "BULLET"){
		enemystate2--;
		if(enemystate2 < 0){
			Destroy(gameObject);
			AudioSource.PlayClipAtPoint(snd, transform.position);
			jsEnemyManager.EnemyNum2--;
			print(jsEnemyManager.EnemyNum2);
			jsScore.score += 30;
		}
	}
	if(coll.gameObject.tag == "BOOMSCALE"){
		enemystate2 -= 100;
		
		if(enemystate2 < 0){
			AudioSource.PlayClipAtPoint(snd, transform.position);
			Destroy(gameObject);
			jsEnemyManager.EnemyNum2--;
			print(jsEnemyManager.EnemyNum2);
			jsScore.score += 30;
		}
	}
}