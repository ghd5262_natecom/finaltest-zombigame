﻿#pragma strict
var snd : AudioClip;
var explosion : Transform;
var boomScale : Transform;
function OnTriggerEnter(coll : Collider)
{
	AudioSource.PlayClipAtPoint(snd, transform.position);
	Destroy(gameObject);
	Instantiate(explosion, transform.position, Quaternion.identity);
	Instantiate(boomScale, transform.position, Quaternion.identity);
	
}